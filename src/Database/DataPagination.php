<?php


namespace Azizyus\LaravelDB\Database;


use App\JobAttachments\Repository\JobAttachmentRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class DataPagination
{

    public function pagination(Builder $query,callable $f,string $queue=null,int $delay=null)
    {
        /**
         * @var LengthAwarePaginator $pagination
         */
        $pagination = $query->paginate(15);
        $querySignature = md5($query->toSql());
        $jobAttachmentRepository = new JobAttachmentRepository();
        while(true)
        {
            foreach ($pagination as $paginationItem)
            {
                //this is very unique signature of
                $signature = $paginationItem->id.'_'.$querySignature.'_'.$queue;
                $z = function()use($f,$paginationItem,$signature,$jobAttachmentRepository){
                    $f($paginationItem);
                };


                if($delay)
                {
                    if(!$jobAttachmentRepository->identifierExist($signature))
                    {
                        $jobAttachmentRepository->placeIdentifierIfItsNotExist($signature);
                        $actualJobF = function()use($z,$jobAttachmentRepository,$signature){
                            $z();
                        };
                        $completed = function() use($jobAttachmentRepository,$signature){
                            $jobAttachmentRepository->removeIdentifier($signature);
                        };

                        dispatch(function() use($actualJobF,$completed) {
                            $actualJobF();
                            $completed();
                        })
                        ->delay(now()->addSeconds($delay))
                        ->onQueue($queue);
                    }
                }
                else
                    $z();

            }
            //fetch next page if there is
            if($pagination->currentPage() < $pagination->lastPage())
                $pagination = $query->paginate(15,'*',null,$pagination->currentPage()+1);
            else
                break;
        }
    }

}
