<?php


namespace Azizyus\LaravelDB\Database;


use Illuminate\Database\Eloquent\Builder;

class WhereAdder
{

    public static function add(array $wheres,Builder $builder)
    {
        foreach ($wheres as $key => $w)
            $builder->withGlobalScope($key,$w);
        return $builder;
    }

}
