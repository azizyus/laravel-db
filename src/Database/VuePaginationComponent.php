<?php


namespace Azizyus\LaravelDB\Database;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;

class VuePaginationComponent
{

    public function paginate(Builder $builder,int $page,$perPage = 20) : Paginator
    {
        return $builder->simplePaginate($perPage,['*'],'page',$page);
    }

    public function paginateLengthAware(Builder $builder,int $page,$perPage = 20,$wheres=[]) : LengthAwarePaginator
    {
        $builder = WhereAdder::add($wheres,$builder);
        return $builder->paginate($perPage,['*'],'page',$page);
    }

}
