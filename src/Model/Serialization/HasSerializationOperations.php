<?php

namespace Azizyus\LaravelDB\Model\Serialization;

trait HasSerializationOperations
{

    /**
     * @var array
     */

    protected $stackedSerializationOperations = [];

    /**
     * @param callable $n
     */
    public function pushSerializationOperation(callable $n)
    {
        $this->stackedSerializationOperations[] = $n;
    }


    /**
     * @param $arr
     * @return mixed
     */
    protected function toArrayOperations($arr)
    {
        foreach ($this->stackedSerializationOperations as $st)
            $arr = $st($arr);
        return $arr;
    }

}
