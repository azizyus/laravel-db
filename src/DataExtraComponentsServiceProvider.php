<?php


namespace Azizyus\LaravelDB;


use Azizyus\LaravelDB\Database\VuePaginationComponent;
use Illuminate\Support\ServiceProvider;

class DataExtraComponentsServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton('vueDataTablePaginationComponent',function(){
            return new VuePaginationComponent();
        });
    }

}
